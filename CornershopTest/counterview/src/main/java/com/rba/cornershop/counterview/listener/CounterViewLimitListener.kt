package com.rba.cornershop.counterview.listener

interface CounterViewLimitListener {

    fun onCounterViewDownLimit()

    fun onCounterViewUpLimit()
}