package com.rba.cornershop.counterview.listener

interface CounterViewUpDownListener {

    fun onUpValue(value: Int)

    fun onDownValue(value: Int)
}