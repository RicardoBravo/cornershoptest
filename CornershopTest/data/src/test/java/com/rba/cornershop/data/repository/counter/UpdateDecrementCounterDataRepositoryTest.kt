package com.rba.cornershop.data.repository.counter

import com.rba.cornershop.data.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UpdateDecrementCounterDataRepositoryTest : BaseTest() {

    private var updateDecrementCounterDataRepository: UpdateDecrementCounterDataRepository? = null

    @Before
    fun setUp() {
        updateDecrementCounterDataRepository =
            mock(UpdateDecrementCounterDataRepository::class.java)
    }

    @Test
    fun updateCounterSuccessfulTest() = runBlocking {
        given(updateDecrementCounterDataRepository?.decrement(updateCounterRequestModel))
            .willReturn(
                generateResultTypeSuccess(
                    list
                )
            )
        updateDecrementCounterDataRepository?.decrement(updateCounterRequestModel)
        verify(updateDecrementCounterDataRepository)?.decrement(updateCounterRequestModel)

        assertNotNull(
            updateDecrementCounterDataRepository?.decrement(
                updateCounterRequestModel
            )
        )
    }

    @Test
    fun updateCounterErrorTest() = runBlocking {
        given(updateDecrementCounterDataRepository?.decrement(updateCounterRequestModel))
            .willReturn(
                generateResultTypeError(
                    error
                )
            )
        updateDecrementCounterDataRepository?.decrement(updateCounterRequestModel)
        verify(updateDecrementCounterDataRepository)?.decrement(updateCounterRequestModel)

        assertNotNull(
            updateDecrementCounterDataRepository?.decrement(
                updateCounterRequestModel
            )
        )
    }
}