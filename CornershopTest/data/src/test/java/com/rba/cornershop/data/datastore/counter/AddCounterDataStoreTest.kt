package com.rba.cornershop.data.datastore.counter

import com.rba.cornershop.data.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AddCounterDataStoreTest : BaseTest() {

    private var addCounterDataStore: AddCounterDataStore? = null

    @Before
    fun setUp() {
        addCounterDataStore = mock(AddCounterDataStore::class.java)
    }

    @Test
    fun addCounterSuccessfulTest() = runBlocking {
        given(addCounterDataStore?.addCounter(addCounterRequest)).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        addCounterDataStore?.addCounter(addCounterRequest)
        verify(addCounterDataStore)?.addCounter(addCounterRequest)

        assertNotNull(addCounterDataStore?.addCounter(addCounterRequest))
    }

    @Test
    fun addCounterErrorTest() = runBlocking {
        given(addCounterDataStore?.addCounter(addCounterRequest)).willReturn(
            generateResultTypeError(
                error
            )
        )
        addCounterDataStore?.addCounter(addCounterRequest)
        verify(addCounterDataStore)?.addCounter(addCounterRequest)

        assertNotNull(addCounterDataStore?.addCounter(addCounterRequest))
    }
}