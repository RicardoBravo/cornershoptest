package com.rba.cornershop.data.datastore.counter

import com.rba.cornershop.data.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UpdateIncrementCounterDataStoreTest : BaseTest() {

    private var updateIncrementCounterDataStore: UpdateIncrementCounterDataStore? = null

    @Before
    fun setUp() {
        updateIncrementCounterDataStore = mock(UpdateIncrementCounterDataStore::class.java)
    }

    @Test
    fun updateDecrementCounterSuccessfulTest() = runBlocking {
        given(updateIncrementCounterDataStore?.updateCounter(updateCounterRequest)).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        updateIncrementCounterDataStore?.updateCounter(updateCounterRequest)
        verify(updateIncrementCounterDataStore)?.updateCounter(updateCounterRequest)

        assertNotNull(updateIncrementCounterDataStore?.updateCounter(updateCounterRequest))
    }

    @Test
    fun updateDecrementCounterErrorTest() = runBlocking {
        given(updateIncrementCounterDataStore?.updateCounter(updateCounterRequest)).willReturn(
            generateResultTypeError(
                error
            )
        )
        updateIncrementCounterDataStore?.updateCounter(updateCounterRequest)
        verify(updateIncrementCounterDataStore)?.updateCounter(updateCounterRequest)

        assertNotNull(updateIncrementCounterDataStore?.updateCounter(updateCounterRequest))
    }
}