package com.rba.cornershop.data.service.counter

import com.rba.cornershop.data.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UpdateIncrementCounterServiceDataStoreTest : BaseTest() {

    private var updateIncrementCounterServiceDataStore: UpdateIncrementCounterServiceDataStore? =
        null

    @Before
    fun setUp() {
        updateIncrementCounterServiceDataStore =
            mock(UpdateIncrementCounterServiceDataStore::class.java)
    }

    @Test
    fun updateCounterTestSuccessful() = runBlocking {
        given(updateIncrementCounterServiceDataStore?.updateCounter(updateCounterRequest))
            .willReturn(
                generateResultTypeSuccess(
                    list
                )
            )
        updateIncrementCounterServiceDataStore?.updateCounter(updateCounterRequest)
        verify(updateIncrementCounterServiceDataStore)?.updateCounter(updateCounterRequest)

        assertNotNull(
            updateIncrementCounterServiceDataStore?.updateCounter(
                updateCounterRequest
            )
        )
    }

    @Test
    fun updateCounterTestError() = runBlocking {
        given(updateIncrementCounterServiceDataStore?.updateCounter(updateCounterRequest))
            .willReturn(
                generateResultTypeError(
                    error
                )
            )
        updateIncrementCounterServiceDataStore?.updateCounter(updateCounterRequest)
        verify(updateIncrementCounterServiceDataStore)?.updateCounter(updateCounterRequest)

        assertNotNull(
            updateIncrementCounterServiceDataStore?.updateCounter(
                updateCounterRequest
            )
        )
    }
}