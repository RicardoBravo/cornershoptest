package com.rba.cornershop.data.repository.counter

import com.rba.cornershop.data.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ListCounterDataRepositoryTest : BaseTest() {

    private var listCounterDataRepository: ListCounterDataRepository? = null

    @Before
    fun setUp() {
        listCounterDataRepository = mock(ListCounterDataRepository::class.java)
    }

    @Test
    fun listCounterSuccessfulTest() = runBlocking {
        given(listCounterDataRepository?.listCounter()).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        listCounterDataRepository?.listCounter()
        verify(listCounterDataRepository)?.listCounter()

        assertNotNull(listCounterDataRepository?.listCounter())
    }

    @Test
    fun listCounterErrorTest() = runBlocking {
        given(listCounterDataRepository?.listCounter()).willReturn(
            generateResultTypeError(
                error
            )
        )
        listCounterDataRepository?.listCounter()
        verify(listCounterDataRepository)?.listCounter()

        assertNotNull(listCounterDataRepository?.listCounter())
    }
}