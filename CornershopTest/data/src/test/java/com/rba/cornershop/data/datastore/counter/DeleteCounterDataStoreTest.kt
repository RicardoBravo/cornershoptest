package com.rba.cornershop.data.datastore.counter

import com.rba.cornershop.data.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DeleteCounterDataStoreTest : BaseTest() {

    private var deleteCounterDataStore: DeleteCounterDataStore? = null

    @Before
    fun setUp() {
        deleteCounterDataStore = mock(DeleteCounterDataStore::class.java)
    }

    @Test
    fun deleteCounterSuccessfulTest() = runBlocking {
        given(deleteCounterDataStore?.delete(updateCounterRequest)).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        deleteCounterDataStore?.delete(updateCounterRequest)
        verify(deleteCounterDataStore)?.delete(updateCounterRequest)

        assertNotNull(deleteCounterDataStore?.delete(updateCounterRequest))
    }

    @Test
    fun deleteCounterErrorTest() = runBlocking {
        given(deleteCounterDataStore?.delete(updateCounterRequest)).willReturn(
            generateResultTypeError(
                error
            )
        )
        deleteCounterDataStore?.delete(updateCounterRequest)
        verify(deleteCounterDataStore)?.delete(updateCounterRequest)

        assertNotNull(deleteCounterDataStore?.delete(updateCounterRequest))
    }
}