package com.rba.cornershop.data.base

import com.rba.cornershop.data.entity.counter.AddCounterRequest
import com.rba.cornershop.data.entity.counter.UpdateCounterRequest
import com.rba.cornershop.domain.model.counter.AddCounterRequestModel
import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.counter.UpdateCounterRequestModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.util.ResultType
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.Spy

@RunWith(MockitoJUnitRunner::class)
abstract class BaseTest {

    @Spy
    var list: List<CounterResponseModel> = ArrayList()
    val error = Mockito.mock(ErrorModel::class.java)
    val addCounterRequestModel = Mockito.mock(AddCounterRequestModel::class.java)
    val addCounterRequest = Mockito.mock(AddCounterRequest::class.java)
    val updateCounterRequestModel = Mockito.mock(UpdateCounterRequestModel::class.java)
    val updateCounterRequest = Mockito.mock(UpdateCounterRequest::class.java)

    fun generateResultTypeSuccess(list: List<CounterResponseModel>):
            ResultType<List<CounterResponseModel>, ErrorModel> {
        return ResultType.Success(list)
    }

    fun generateResultTypeError(errorModel: ErrorModel):
            ResultType<List<CounterResponseModel>, ErrorModel> {
        return ResultType.Error(errorModel)
    }
}