package com.rba.cornershop.data.repository.counter

import com.rba.cornershop.data.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DeleteCounterDataRepositoryTest : BaseTest() {

    private var deleteCounterDataRepository: DeleteCounterDataRepository? = null

    @Before
    fun setUp() {
        deleteCounterDataRepository = mock(DeleteCounterDataRepository::class.java)
    }

    @Test
    fun deleteCounterSuccessfulTest() = runBlocking {
        given(deleteCounterDataRepository?.delete(updateCounterRequestModel)).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        deleteCounterDataRepository?.delete(updateCounterRequestModel)
        verify(deleteCounterDataRepository)?.delete(updateCounterRequestModel)

        assertNotNull(deleteCounterDataRepository?.delete(updateCounterRequestModel))
    }

    @Test
    fun deleteCounterErrorTest() = runBlocking {
        given(deleteCounterDataRepository?.delete(updateCounterRequestModel)).willReturn(
            generateResultTypeError(
                error
            )
        )
        deleteCounterDataRepository?.delete(updateCounterRequestModel)
        verify(deleteCounterDataRepository)?.delete(updateCounterRequestModel)

        assertNotNull(deleteCounterDataRepository?.delete(updateCounterRequestModel))
    }
}