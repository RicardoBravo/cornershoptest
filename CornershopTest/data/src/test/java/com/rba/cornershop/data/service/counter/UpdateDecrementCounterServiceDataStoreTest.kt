package com.rba.cornershop.data.service.counter

import com.rba.cornershop.data.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UpdateDecrementCounterServiceDataStoreTest : BaseTest() {

    private var updateDecrementCounterServiceDataStore: UpdateDecrementCounterServiceDataStore? =
        null

    @Before
    fun setUp() {
        updateDecrementCounterServiceDataStore =
            mock(UpdateDecrementCounterServiceDataStore::class.java)
    }

    @Test
    fun updateCounterTestSuccessful() = runBlocking {
        given(updateDecrementCounterServiceDataStore?.updateCounter(updateCounterRequest))
            .willReturn(
                generateResultTypeSuccess(
                    list
                )
            )
        updateDecrementCounterServiceDataStore?.updateCounter(updateCounterRequest)
        verify(updateDecrementCounterServiceDataStore)?.updateCounter(updateCounterRequest)

        assertNotNull(
            updateDecrementCounterServiceDataStore?.updateCounter(
                updateCounterRequest
            )
        )
    }

    @Test
    fun updateCounterTestError() = runBlocking {
        given(updateDecrementCounterServiceDataStore?.updateCounter(updateCounterRequest))
            .willReturn(
                generateResultTypeError(
                    error
                )
            )
        updateDecrementCounterServiceDataStore?.updateCounter(updateCounterRequest)
        verify(updateDecrementCounterServiceDataStore)?.updateCounter(updateCounterRequest)

        assertNotNull(
            updateDecrementCounterServiceDataStore?.updateCounter(
                updateCounterRequest
            )
        )
    }
}