package com.rba.cornershop.data.repository.counter

import com.rba.cornershop.data.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UpdateIncrementCounterDataRepositoryTest : BaseTest() {

    private var updateIncrementCounterDataRepository: UpdateIncrementCounterDataRepository? = null

    @Before
    fun setUp() {
        updateIncrementCounterDataRepository =
            mock(UpdateIncrementCounterDataRepository::class.java)
    }

    @Test
    fun updateCounterSuccessfulTest() = runBlocking {
        given(updateIncrementCounterDataRepository?.increment(updateCounterRequestModel))
            .willReturn(
                generateResultTypeSuccess(
                    list
                )
            )
        updateIncrementCounterDataRepository?.increment(updateCounterRequestModel)
        verify(updateIncrementCounterDataRepository)?.increment(updateCounterRequestModel)

        assertNotNull(
            updateIncrementCounterDataRepository?.increment(
                updateCounterRequestModel
            )
        )
    }

    @Test
    fun updateCounterErrorTest() = runBlocking {
        given(updateIncrementCounterDataRepository?.increment(updateCounterRequestModel))
            .willReturn(
                generateResultTypeError(
                    error
                )
            )
        updateIncrementCounterDataRepository?.increment(updateCounterRequestModel)
        verify(updateIncrementCounterDataRepository)?.increment(updateCounterRequestModel)

        assertNotNull(
            updateIncrementCounterDataRepository?.increment(
                updateCounterRequestModel
            )
        )
    }
}