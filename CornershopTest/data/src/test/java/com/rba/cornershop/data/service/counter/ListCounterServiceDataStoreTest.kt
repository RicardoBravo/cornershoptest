package com.rba.cornershop.data.service.counter

import com.rba.cornershop.data.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ListCounterServiceDataStoreTest : BaseTest() {

    private var listCounterServiceDataStore: ListCounterServiceDataStore? = null

    @Before
    fun setUp() {
        listCounterServiceDataStore = mock(ListCounterServiceDataStore::class.java)
    }

    @Test
    fun listCounterTestSuccessful() = runBlocking {
        given(listCounterServiceDataStore?.listCounter()).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        listCounterServiceDataStore?.listCounter()
        verify(listCounterServiceDataStore)?.listCounter()

        assertNotNull(listCounterServiceDataStore?.listCounter())
    }

    @Test
    fun listCounterTestError() = runBlocking {
        given(listCounterServiceDataStore?.listCounter()).willReturn(
            generateResultTypeError(
                error
            )
        )
        listCounterServiceDataStore?.listCounter()
        verify(listCounterServiceDataStore)?.listCounter()

        assertNotNull(listCounterServiceDataStore?.listCounter())
    }
}