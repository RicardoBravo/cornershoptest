package com.rba.cornershop.data.service.counter

import com.rba.cornershop.data.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DeleteCounterServiceDataStoreTest : BaseTest() {

    private var deleteCounterServiceDataStore: DeleteCounterServiceDataStore? = null

    @Before
    fun setUp() {
        deleteCounterServiceDataStore = mock(DeleteCounterServiceDataStore::class.java)
    }

    @Test
    fun deleteCounterTestSuccessful() = runBlocking {
        given(deleteCounterServiceDataStore?.delete(updateCounterRequest)).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        deleteCounterServiceDataStore?.delete(updateCounterRequest)
        verify(deleteCounterServiceDataStore)?.delete(updateCounterRequest)

        assertNotNull(deleteCounterServiceDataStore?.delete(updateCounterRequest))
    }

    @Test
    fun deleteCounterTestError() = runBlocking {
        given(deleteCounterServiceDataStore?.delete(updateCounterRequest)).willReturn(
            generateResultTypeError(
                error
            )
        )
        deleteCounterServiceDataStore?.delete(updateCounterRequest)
        verify(deleteCounterServiceDataStore)?.delete(updateCounterRequest)

        assertNotNull(deleteCounterServiceDataStore?.delete(updateCounterRequest))
    }
}