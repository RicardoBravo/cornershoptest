package com.rba.cornershop.data.datastore.counter

import com.rba.cornershop.data.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UpdateDecrementCounterDataStoreTest : BaseTest() {

    private var updateDecrementCounterDataStore: UpdateDecrementCounterDataStore? = null

    @Before
    fun setUp() {
        updateDecrementCounterDataStore = mock(UpdateDecrementCounterDataStore::class.java)
    }

    @Test
    fun updateDecrementCounterSuccessfulTest() = runBlocking {
        given(updateDecrementCounterDataStore?.updateCounter(updateCounterRequest)).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        updateDecrementCounterDataStore?.updateCounter(updateCounterRequest)
        verify(updateDecrementCounterDataStore)?.updateCounter(updateCounterRequest)

        assertNotNull(updateDecrementCounterDataStore?.updateCounter(updateCounterRequest))
    }

    @Test
    fun updateDecrementCounterErrorTest() = runBlocking {
        given(updateDecrementCounterDataStore?.updateCounter(updateCounterRequest)).willReturn(
            generateResultTypeError(
                error
            )
        )
        updateDecrementCounterDataStore?.updateCounter(updateCounterRequest)
        verify(updateDecrementCounterDataStore)?.updateCounter(updateCounterRequest)

        assertNotNull(updateDecrementCounterDataStore?.updateCounter(updateCounterRequest))
    }
}