package com.rba.cornershop.data.service.counter

import com.rba.cornershop.data.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AddCounterServiceDataStoreTest : BaseTest() {

    private var addCounterServiceDataStore: AddCounterServiceDataStore? = null

    @Before
    fun setUp() {
        addCounterServiceDataStore = mock(AddCounterServiceDataStore::class.java)
    }

    @Test
    fun addCounterTestSuccessful() = runBlocking {
        given(addCounterServiceDataStore?.addCounter(addCounterRequest)).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        addCounterServiceDataStore?.addCounter(addCounterRequest)
        verify(addCounterServiceDataStore)?.addCounter(addCounterRequest)

        assertNotNull(addCounterServiceDataStore?.addCounter(addCounterRequest))
    }

    @Test
    fun addCounterTestError() = runBlocking {
        given(addCounterServiceDataStore?.addCounter(addCounterRequest)).willReturn(
            generateResultTypeError(
                error
            )
        )
        addCounterServiceDataStore?.addCounter(addCounterRequest)
        verify(addCounterServiceDataStore)?.addCounter(addCounterRequest)

        assertNotNull(addCounterServiceDataStore?.addCounter(addCounterRequest))
    }
}