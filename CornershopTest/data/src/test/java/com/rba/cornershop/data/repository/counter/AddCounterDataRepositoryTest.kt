package com.rba.cornershop.data.repository.counter

import com.rba.cornershop.data.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AddCounterDataRepositoryTest : BaseTest() {

    private var addCounterDataRepository: AddCounterDataRepository? = null

    @Before
    fun setUp() {
        addCounterDataRepository = mock(AddCounterDataRepository::class.java)
    }

    @Test
    fun addCounterSuccessfulTest() = runBlocking {
        given(addCounterDataRepository?.addCounter(addCounterRequestModel)).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        addCounterDataRepository?.addCounter(addCounterRequestModel)
        verify(addCounterDataRepository)?.addCounter(addCounterRequestModel)

        assertNotNull(addCounterDataRepository?.addCounter(addCounterRequestModel))
    }

    @Test
    fun addCounterErrorTest() = runBlocking {
        given(addCounterDataRepository?.addCounter(addCounterRequestModel)).willReturn(
            generateResultTypeError(
                error
            )
        )
        addCounterDataRepository?.addCounter(addCounterRequestModel)
        verify(addCounterDataRepository)?.addCounter(addCounterRequestModel)

        assertNotNull(addCounterDataRepository?.addCounter(addCounterRequestModel))
    }
}