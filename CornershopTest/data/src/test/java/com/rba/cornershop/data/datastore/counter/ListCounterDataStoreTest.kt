package com.rba.cornershop.data.datastore.counter

import com.rba.cornershop.data.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ListCounterDataStoreTest : BaseTest() {

    private var listCounterDataStore: ListCounterDataStore? = null

    @Before
    fun setUp() {
        listCounterDataStore = mock(ListCounterDataStore::class.java)
    }

    @Test
    fun listCounterSuccessfulTest() = runBlocking {
        given(listCounterDataStore?.listCounter()).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        listCounterDataStore?.listCounter()
        verify(listCounterDataStore)?.listCounter()

        assertNotNull(listCounterDataStore?.listCounter())
    }

    @Test
    fun listCounterErrorTest() = runBlocking {
        given(listCounterDataStore?.listCounter()).willReturn(
            generateResultTypeError(
                error
            )
        )
        listCounterDataStore?.listCounter()
        verify(listCounterDataStore)?.listCounter()

        assertNotNull(listCounterDataStore?.listCounter())
    }
}