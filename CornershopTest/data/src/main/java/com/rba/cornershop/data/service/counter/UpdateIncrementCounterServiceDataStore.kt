package com.rba.cornershop.data.service.counter

import com.rba.cornershop.data.api.ApiManager
import com.rba.cornershop.data.datastore.counter.UpdateIncrementCounterDataStore
import com.rba.cornershop.data.entity.counter.UpdateCounterRequest
import com.rba.cornershop.data.mapper.counter.CounterResponseMapper
import com.rba.cornershop.data.mapper.error.ErrorMapper
import com.rba.cornershop.data.util.ErrorUtil
import com.rba.cornershop.data.util.RetrofitErrorUtil
import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.util.ResultType

class UpdateIncrementCounterServiceDataStore : UpdateIncrementCounterDataStore {

    override suspend fun updateCounter(updateCounterRequest: UpdateCounterRequest): ResultType<List<CounterResponseModel>, ErrorModel> {
        return try {
            val response =
                ApiManager.apiManager().updateIncrementCounter(updateCounterRequest).await()

            return if (response.isSuccessful) {
                val counterResponse = response.body()
                ResultType.Success(CounterResponseMapper.transform(counterResponse!!))
            } else {
                val error = RetrofitErrorUtil.parseError(response)!!
                ResultType.Error(ErrorMapper.transform(error))
            }
        } catch (t: Throwable) {
            ResultType.Error(ErrorUtil.errorHandler(t))
        }
    }
}