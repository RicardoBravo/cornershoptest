package com.rba.cornershop.data.repository.counter

import com.rba.cornershop.data.mapper.counter.UpdateCounterRequestMapper
import com.rba.cornershop.data.service.counter.DeleteCounterServiceDataStore
import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.counter.UpdateCounterRequestModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.repository.counter.DeleteCounterRepository
import com.rba.cornershop.domain.util.ResultType

open class DeleteCounterDataRepository :
    DeleteCounterRepository {

    override suspend fun delete(updateCounterRequestModel: UpdateCounterRequestModel): ResultType<List<CounterResponseModel>, ErrorModel> {
        val deleteCounterServiceDataStore = DeleteCounterServiceDataStore()
        return deleteCounterServiceDataStore.delete(
            UpdateCounterRequestMapper.transform(
                updateCounterRequestModel
            )
        )
    }
}