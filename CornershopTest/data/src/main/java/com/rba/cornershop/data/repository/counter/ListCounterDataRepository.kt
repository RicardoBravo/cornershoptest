package com.rba.cornershop.data.repository.counter

import com.rba.cornershop.data.service.counter.ListCounterServiceDataStore
import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.repository.counter.ListCounterRepository
import com.rba.cornershop.domain.util.ResultType

class ListCounterDataRepository :
    ListCounterRepository {

    override suspend fun listCounter(): ResultType<List<CounterResponseModel>, ErrorModel> {
        val listCounterServiceDataStore = ListCounterServiceDataStore()
        return listCounterServiceDataStore.listCounter()
    }
}