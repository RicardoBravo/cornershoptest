package com.rba.cornershop.data.datastore.counter

import com.rba.cornershop.data.entity.counter.UpdateCounterRequest
import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.util.ResultType

interface UpdateIncrementCounterDataStore {
    suspend fun updateCounter(updateCounterRequest: UpdateCounterRequest): ResultType<List<CounterResponseModel>, ErrorModel>
}