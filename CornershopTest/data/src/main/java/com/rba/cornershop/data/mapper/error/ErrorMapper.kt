package com.rba.cornershop.data.mapper.error

import com.rba.cornershop.data.entity.error.ErrorResponse
import com.rba.cornershop.domain.model.error.ErrorModel

object ErrorMapper {

    fun transform(errorResponse: ErrorResponse): ErrorModel {

        val errorModel = ErrorModel()
        errorModel.code = errorResponse.code
        errorModel.message = errorResponse.message
        errorModel.status = errorResponse.status

        return errorModel
    }
}