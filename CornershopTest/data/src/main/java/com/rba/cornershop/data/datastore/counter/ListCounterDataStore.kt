package com.rba.cornershop.data.datastore.counter

import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.util.ResultType

interface ListCounterDataStore {

    suspend fun listCounter(): ResultType<List<CounterResponseModel>, ErrorModel>
}