package com.rba.cornershop.data.entity.counter

data class CounterResponse(

    /**
     * id : k6ttsc9s
     * title : Test
     * count : 0
     */

    var id: String? = null,
    var title: String? = null,
    var count: Int = 0
)