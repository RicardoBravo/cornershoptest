package com.rba.cornershop.data.repository.counter

import com.rba.cornershop.data.mapper.counter.AddCounterRequestMapper
import com.rba.cornershop.data.service.counter.AddCounterServiceDataStore
import com.rba.cornershop.domain.model.counter.AddCounterRequestModel
import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.repository.counter.AddCounterRepository
import com.rba.cornershop.domain.util.ResultType

class AddCounterDataRepository : AddCounterRepository {

    override suspend fun addCounter(addCounterRequestModel: AddCounterRequestModel): ResultType<List<CounterResponseModel>, ErrorModel> {
        val addCounterServiceDataStore = AddCounterServiceDataStore()
        return addCounterServiceDataStore.addCounter(
            AddCounterRequestMapper.transform(
                addCounterRequestModel
            )
        )
    }
}