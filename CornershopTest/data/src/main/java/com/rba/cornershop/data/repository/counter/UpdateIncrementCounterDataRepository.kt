package com.rba.cornershop.data.repository.counter

import com.rba.cornershop.data.mapper.counter.UpdateCounterRequestMapper
import com.rba.cornershop.data.service.counter.UpdateIncrementCounterServiceDataStore
import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.counter.UpdateCounterRequestModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.repository.counter.UpdateIncrementCounterRepository
import com.rba.cornershop.domain.util.ResultType

open class UpdateIncrementCounterDataRepository :
    UpdateIncrementCounterRepository {

    override suspend fun increment(updateCounterRequestModel: UpdateCounterRequestModel):
            ResultType<List<CounterResponseModel>, ErrorModel> {
        val updateIncrementCounterServiceDataStore = UpdateIncrementCounterServiceDataStore()
        return updateIncrementCounterServiceDataStore.updateCounter(
            UpdateCounterRequestMapper.transform(
                updateCounterRequestModel
            )
        )
    }
}