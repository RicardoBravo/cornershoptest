package com.rba.cornershop.data.entity.counter

data class UpdateCounterRequest(

    /**
     * id : k6sm521q
     */

    var id: String? = null
)