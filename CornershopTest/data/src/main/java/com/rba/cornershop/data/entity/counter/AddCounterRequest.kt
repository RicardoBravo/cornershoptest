package com.rba.cornershop.data.entity.counter

data class AddCounterRequest(

    /**
     * title : Test
     */

    var title: String? = null
)