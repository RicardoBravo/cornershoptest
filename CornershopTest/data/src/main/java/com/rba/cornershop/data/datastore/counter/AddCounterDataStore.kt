package com.rba.cornershop.data.datastore.counter

import com.rba.cornershop.data.entity.counter.AddCounterRequest
import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.util.ResultType

interface AddCounterDataStore {

    suspend fun addCounter(addCounterRequest: AddCounterRequest): ResultType<List<CounterResponseModel>, ErrorModel>
}