package com.rba.cornershop.data.mapper.counter

import com.rba.cornershop.data.entity.counter.UpdateCounterRequest
import com.rba.cornershop.domain.model.counter.UpdateCounterRequestModel

object UpdateCounterRequestMapper {

    fun transform(updateCounterRequest: UpdateCounterRequest): UpdateCounterRequestModel {

        val updateCounterRequestModel = UpdateCounterRequestModel()
        updateCounterRequestModel.id = updateCounterRequest.id

        return updateCounterRequestModel
    }

    fun transform(updateCounterRequestModel: UpdateCounterRequestModel): UpdateCounterRequest {

        val updateCounterRequest = UpdateCounterRequest()
        updateCounterRequest.id = updateCounterRequestModel.id

        return updateCounterRequest
    }
}