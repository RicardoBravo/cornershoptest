package com.rba.cornershop.data.entity.error

data class ErrorResponse(
    var code: Int = 0,
    var status: String? = null,
    var message: String? = null
)