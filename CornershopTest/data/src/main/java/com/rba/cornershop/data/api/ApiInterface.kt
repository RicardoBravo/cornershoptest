package com.rba.cornershop.data.api

import com.rba.cornershop.data.entity.counter.AddCounterRequest
import com.rba.cornershop.data.entity.counter.CounterResponse
import com.rba.cornershop.data.entity.counter.UpdateCounterRequest
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface {

    @POST("counter")
    fun addCounter(@Body addCounterRequest: AddCounterRequest): Deferred<Response<List<CounterResponse>>>

    @POST("counter/dec")
    fun updateDecrementCounter(@Body updateCounterRequest: UpdateCounterRequest): Deferred<Response<List<CounterResponse>>>

    @POST("counter/inc")
    fun updateIncrementCounter(@Body updateCounterRequest: UpdateCounterRequest): Deferred<Response<List<CounterResponse>>>

    @GET("counters")
    fun listCounter(): Deferred<Response<List<CounterResponse>>>

    @HTTP(method = "DELETE", path = "counter", hasBody = true)
    fun delete(@Body updateCounterRequest: UpdateCounterRequest): Deferred<Response<List<CounterResponse>>>
}