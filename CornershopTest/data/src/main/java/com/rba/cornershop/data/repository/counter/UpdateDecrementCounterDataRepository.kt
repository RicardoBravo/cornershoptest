package com.rba.cornershop.data.repository.counter

import com.rba.cornershop.data.mapper.counter.UpdateCounterRequestMapper
import com.rba.cornershop.data.service.counter.UpdateDecrementCounterServiceDataStore
import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.counter.UpdateCounterRequestModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.repository.counter.UpdateDecrementCounterRepository
import com.rba.cornershop.domain.util.ResultType

open class UpdateDecrementCounterDataRepository :
    UpdateDecrementCounterRepository {

    override suspend fun decrement(updateCounterRequestModel: UpdateCounterRequestModel):
            ResultType<List<CounterResponseModel>, ErrorModel> {
        val updateDecrementCounterServiceDataStore = UpdateDecrementCounterServiceDataStore()
        return updateDecrementCounterServiceDataStore.updateCounter(
            UpdateCounterRequestMapper.transform(
                updateCounterRequestModel
            )
        )
    }
}