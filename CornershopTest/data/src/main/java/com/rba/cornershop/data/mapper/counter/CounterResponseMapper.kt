package com.rba.cornershop.data.mapper.counter

import com.rba.cornershop.data.entity.counter.CounterResponse
import com.rba.cornershop.domain.model.counter.CounterResponseModel

object CounterResponseMapper {

    fun transform(counterResponseList: List<CounterResponse>): List<CounterResponseModel> {

        val counterResponseModelList = arrayListOf<CounterResponseModel>()

        counterResponseList.map {
            counterResponseModelList.add(transform(it))
        }

        return counterResponseModelList
    }

    fun transform(counterResponse: CounterResponse): CounterResponseModel {

        val counterResponseModel = CounterResponseModel()
        counterResponseModel.id = counterResponse.id
        counterResponseModel.title = counterResponse.title
        counterResponseModel.count = counterResponse.count

        return counterResponseModel
    }
}