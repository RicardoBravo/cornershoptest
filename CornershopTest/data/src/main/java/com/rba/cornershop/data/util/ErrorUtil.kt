package com.rba.cornershop.data.util

import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.util.ConstantError

object ErrorUtil {

    fun errorHandler(error: Throwable): ErrorModel {

        val errorException: RetrofitException =
            if (error is RetrofitException) {
                error
            } else {
                RetrofitException.retrofitException(error)
            }

        return when (errorException.kind) {
            RetrofitException.Kind.HTTP -> errorException.getErrorBodyAs(ErrorModel::class.java)!!
            RetrofitException.Kind.NETWORK -> ErrorModel()
            else -> ErrorModel(0, "", ConstantError.ERROR)
        }
    }
}