package com.rba.cornershop.data.mapper.counter

import com.rba.cornershop.data.entity.counter.AddCounterRequest
import com.rba.cornershop.domain.model.counter.AddCounterRequestModel

object AddCounterRequestMapper {

    fun transform(addCounterRequest: AddCounterRequest): AddCounterRequestModel {

        val addCounterRequestModel = AddCounterRequestModel()
        addCounterRequestModel.title = addCounterRequest.title

        return addCounterRequestModel
    }

    fun transform(addCounterRequestModel: AddCounterRequestModel): AddCounterRequest {

        val aaCounterRequest = AddCounterRequest()
        aaCounterRequest.title = addCounterRequestModel.title

        return aaCounterRequest
    }
}