package com.rba.cornershop.domain.usecase.counter

import com.rba.cornershop.domain.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UpdateDecrementCounterUseCaseTest : BaseTest() {

    private var updateDecrementCounterUseCase: UpdateDecrementCounterUseCase? = null

    @Before
    fun setUp() {
        updateDecrementCounterUseCase = mock(UpdateDecrementCounterUseCase::class.java)
    }

    @Test
    fun updateCounterSuccessfulTest() = runBlocking {
        given(updateDecrementCounterUseCase?.updateCounter(updateCounterRequestModel)).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        updateDecrementCounterUseCase?.updateCounter(updateCounterRequestModel)
        verify(updateDecrementCounterUseCase)?.updateCounter(updateCounterRequestModel)

        assertNotNull(updateDecrementCounterUseCase?.updateCounter(updateCounterRequestModel))
    }

    @Test
    fun updateCounterErrorTest() = runBlocking {
        given(updateDecrementCounterUseCase?.updateCounter(updateCounterRequestModel)).willReturn(
            generateResultTypeError(
                error
            )
        )
        updateDecrementCounterUseCase?.updateCounter(updateCounterRequestModel)
        verify(updateDecrementCounterUseCase)?.updateCounter(updateCounterRequestModel)

        assertNotNull(updateDecrementCounterUseCase?.updateCounter(updateCounterRequestModel))
    }
}