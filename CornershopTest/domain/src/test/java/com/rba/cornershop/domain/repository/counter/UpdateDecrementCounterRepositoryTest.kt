package com.rba.cornershop.domain.repository.counter

import com.rba.cornershop.domain.base.BaseTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UpdateDecrementCounterRepositoryTest : BaseTest() {

    private var updateDecrementCounterRepository: UpdateDecrementCounterRepository? = null

    @Before
    fun setUp() {
        updateDecrementCounterRepository =
            mock(UpdateDecrementCounterRepository::class.java)
    }

    @Test
    fun updateCounterSuccessfulTest() = runBlocking {
        given(updateDecrementCounterRepository?.decrement(updateCounterRequestModel))
            .willReturn(
                generateResultTypeSuccess(
                    list
                )
            )
        updateDecrementCounterRepository?.decrement(updateCounterRequestModel)
        verify(updateDecrementCounterRepository)?.decrement(updateCounterRequestModel)

        assertNotNull(updateDecrementCounterRepository?.decrement(updateCounterRequestModel))
    }

    @Test
    fun updateCounterErrorTest() = runBlocking {
        given(updateDecrementCounterRepository?.decrement(updateCounterRequestModel))
            .willReturn(
                generateResultTypeError(
                    error
                )
            )
        updateDecrementCounterRepository?.decrement(updateCounterRequestModel)
        verify(updateDecrementCounterRepository)?.decrement(updateCounterRequestModel)

        assertNotNull(updateDecrementCounterRepository?.decrement(updateCounterRequestModel))
    }
}