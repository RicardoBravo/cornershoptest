package com.rba.cornershop.domain.repository.counter

import com.rba.cornershop.domain.base.BaseTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AddCounterRepositoryTest : BaseTest() {

    private var addCounterRepository: AddCounterRepository? = null

    @Before
    fun setUp() {
        addCounterRepository = mock(AddCounterRepository::class.java)
    }

    @Test
    fun addCounterSuccessfulTest() = runBlocking {
        given(addCounterRepository?.addCounter(addCounterRequestModel)).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        addCounterRepository?.addCounter(addCounterRequestModel)
        verify(addCounterRepository)?.addCounter(addCounterRequestModel)

        assertNotNull(addCounterRepository?.addCounter(addCounterRequestModel))
    }

    @Test
    fun addCounterErrorTest() = runBlocking {
        given(addCounterRepository?.addCounter(addCounterRequestModel)).willReturn(
            generateResultTypeError(
                error
            )
        )
        addCounterRepository?.addCounter(addCounterRequestModel)
        verify(addCounterRepository)?.addCounter(addCounterRequestModel)

        assertNotNull(addCounterRepository?.addCounter(addCounterRequestModel))
    }
}