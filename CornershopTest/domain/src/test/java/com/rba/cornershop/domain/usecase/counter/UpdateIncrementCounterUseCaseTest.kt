package com.rba.cornershop.domain.usecase.counter

import com.rba.cornershop.domain.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UpdateIncrementCounterUseCaseTest : BaseTest() {

    private var updateIncrementCounterUseCase: UpdateIncrementCounterUseCase? = null

    @Before
    fun setUp() {
        updateIncrementCounterUseCase = mock(UpdateIncrementCounterUseCase::class.java)
    }

    @Test
    fun updateCounterSuccessfulTest() = runBlocking {
        given(updateIncrementCounterUseCase?.updateCounter(updateCounterRequestModel)).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        updateIncrementCounterUseCase?.updateCounter(updateCounterRequestModel)
        verify(updateIncrementCounterUseCase)?.updateCounter(updateCounterRequestModel)

        assertNotNull(updateIncrementCounterUseCase?.updateCounter(updateCounterRequestModel))
    }

    @Test
    fun updateCounterErrorTest() = runBlocking {
        given(updateIncrementCounterUseCase?.updateCounter(updateCounterRequestModel)).willReturn(
            generateResultTypeError(
                error
            )
        )
        updateIncrementCounterUseCase?.updateCounter(updateCounterRequestModel)
        verify(updateIncrementCounterUseCase)?.updateCounter(updateCounterRequestModel)

        assertNotNull(updateIncrementCounterUseCase?.updateCounter(updateCounterRequestModel))
    }
}