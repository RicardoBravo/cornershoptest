package com.rba.cornershop.domain.repository.counter

import com.rba.cornershop.domain.base.BaseTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DeleteCounterRepositoryTest : BaseTest() {

    private var deleteCounterRepository: DeleteCounterRepository? = null

    @Before
    fun setUp() {
        deleteCounterRepository = mock(DeleteCounterRepository::class.java)
    }

    @Test
    fun deleteCounterSuccessfulTest() = runBlocking {
        given(deleteCounterRepository?.delete(updateCounterRequestModel)).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        deleteCounterRepository?.delete(updateCounterRequestModel)
        verify(deleteCounterRepository)?.delete(updateCounterRequestModel)

        assertNotNull(deleteCounterRepository?.delete(updateCounterRequestModel))
    }

    @Test
    fun deleteCounterErrorTest() = runBlocking {
        given(deleteCounterRepository?.delete(updateCounterRequestModel)).willReturn(
            generateResultTypeError(
                error
            )
        )
        deleteCounterRepository?.delete(updateCounterRequestModel)
        verify(deleteCounterRepository)?.delete(updateCounterRequestModel)

        assertNotNull(deleteCounterRepository?.delete(updateCounterRequestModel))
    }
}