package com.rba.cornershop.domain.repository.counter

import com.rba.cornershop.domain.base.BaseTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ListCounterRepositoryTest : BaseTest() {

    private var listCounterRepository: ListCounterRepository? = null

    @Before
    fun setUp() {
        listCounterRepository = mock(ListCounterRepository::class.java)
    }

    @Test
    fun listCounterSuccessfulTest() = runBlocking {
        given(listCounterRepository?.listCounter()).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        listCounterRepository?.listCounter()
        verify(listCounterRepository)?.listCounter()

        assertNotNull(listCounterRepository?.listCounter())
    }

    @Test
    fun listCounterErrorTest() = runBlocking {
        given(listCounterRepository?.listCounter()).willReturn(
            generateResultTypeError(
                error
            )
        )
        listCounterRepository?.listCounter()
        verify(listCounterRepository)?.listCounter()

        assertNotNull(listCounterRepository?.listCounter())
    }
}