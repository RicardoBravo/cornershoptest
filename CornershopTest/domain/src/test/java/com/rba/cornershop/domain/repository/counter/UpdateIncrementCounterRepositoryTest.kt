package com.rba.cornershop.domain.repository.counter

import com.rba.cornershop.domain.base.BaseTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UpdateIncrementCounterRepositoryTest : BaseTest() {

    private var updateIncrementCounterRepository: UpdateIncrementCounterRepository? = null

    @Before
    fun setUp() {
        updateIncrementCounterRepository =
            mock(UpdateIncrementCounterRepository::class.java)
    }

    @Test
    fun updateCounterSuccessfulTest() = runBlocking {
        given(updateIncrementCounterRepository?.increment(updateCounterRequestModel))
            .willReturn(
                generateResultTypeSuccess(
                    list
                )
            )
        updateIncrementCounterRepository?.increment(updateCounterRequestModel)
        verify(updateIncrementCounterRepository)?.increment(updateCounterRequestModel)

        assertNotNull(updateIncrementCounterRepository?.increment(updateCounterRequestModel))
    }

    @Test
    fun updateCounterErrorTest() = runBlocking {
        given(updateIncrementCounterRepository?.increment(updateCounterRequestModel))
            .willReturn(
                generateResultTypeError(
                    error
                )
            )
        updateIncrementCounterRepository?.increment(updateCounterRequestModel)
        verify(updateIncrementCounterRepository)?.increment(updateCounterRequestModel)

        assertNotNull(updateIncrementCounterRepository?.increment(updateCounterRequestModel))
    }
}