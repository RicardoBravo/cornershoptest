package com.rba.cornershop.domain.usecase.counter

import com.rba.cornershop.domain.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DeleteCounterUseCaseTest : BaseTest() {

    private var deleteCounterUseCase: DeleteCounterUseCase? = null

    @Before
    fun setUp() {
        deleteCounterUseCase = mock(DeleteCounterUseCase::class.java)
    }

    @Test
    fun deleteCounterSuccessfulTest() = runBlocking {
        given(deleteCounterUseCase?.deleteCounter(updateCounterRequestModel)).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        deleteCounterUseCase?.deleteCounter(updateCounterRequestModel)
        verify(deleteCounterUseCase)?.deleteCounter(updateCounterRequestModel)

        assertNotNull(deleteCounterUseCase?.deleteCounter(updateCounterRequestModel))
    }

    @Test
    fun deleteCounterErrorTest() = runBlocking {
        given(deleteCounterUseCase?.deleteCounter(updateCounterRequestModel)).willReturn(
            generateResultTypeError(
                error
            )
        )
        deleteCounterUseCase?.deleteCounter(updateCounterRequestModel)
        verify(deleteCounterUseCase)?.deleteCounter(updateCounterRequestModel)

        assertNotNull(deleteCounterUseCase?.deleteCounter(updateCounterRequestModel))
    }
}