package com.rba.cornershop.domain.usecase.counter

import com.rba.cornershop.domain.base.BaseTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AddCounterUseCaseTest : BaseTest() {

    private var addCounterUseCase: AddCounterUseCase? = null

    @Before
    fun setUp() {
        addCounterUseCase = mock(AddCounterUseCase::class.java)
    }

    @Test
    fun addCounterSuccessfulTest() = runBlocking {
        given(addCounterUseCase?.addCounter(addCounterRequestModel)).willReturn(
            generateResultTypeSuccess(
                list
            )
        )
        addCounterUseCase?.addCounter(addCounterRequestModel)
        verify(addCounterUseCase)?.addCounter(addCounterRequestModel)

        assertNotNull(addCounterUseCase?.addCounter(addCounterRequestModel))
    }

    @Test
    fun addCounterErrorTest() = runBlocking {
        given(addCounterUseCase?.addCounter(addCounterRequestModel)).willReturn(
            generateResultTypeError(
                error
            )
        )
        addCounterUseCase?.addCounter(addCounterRequestModel)
        verify(addCounterUseCase)?.addCounter(addCounterRequestModel)

        assertNotNull(addCounterUseCase?.addCounter(addCounterRequestModel))
    }
}