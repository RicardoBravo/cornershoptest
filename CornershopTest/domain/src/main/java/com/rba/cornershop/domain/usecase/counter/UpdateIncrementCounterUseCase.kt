package com.rba.cornershop.domain.usecase.counter

import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.counter.UpdateCounterRequestModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.repository.counter.UpdateIncrementCounterRepository
import com.rba.cornershop.domain.util.ResultType

class UpdateIncrementCounterUseCase(private val updateIncrementCounterRepository: UpdateIncrementCounterRepository) {

    suspend fun updateCounter(
        updateCounterRequestModel: UpdateCounterRequestModel
    ): ResultType<List<CounterResponseModel>, ErrorModel> {
        return updateIncrementCounterRepository.increment(updateCounterRequestModel)
    }
}