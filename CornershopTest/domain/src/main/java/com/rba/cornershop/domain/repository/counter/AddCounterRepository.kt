package com.rba.cornershop.domain.repository.counter

import com.rba.cornershop.domain.model.counter.AddCounterRequestModel
import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.util.ResultType

interface AddCounterRepository {

    suspend fun addCounter(addCounterRequestModel: AddCounterRequestModel): ResultType<List<CounterResponseModel>, ErrorModel>
}