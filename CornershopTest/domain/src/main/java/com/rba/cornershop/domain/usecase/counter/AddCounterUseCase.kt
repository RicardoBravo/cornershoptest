package com.rba.cornershop.domain.usecase.counter

import com.rba.cornershop.domain.model.counter.AddCounterRequestModel
import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.repository.counter.AddCounterRepository
import com.rba.cornershop.domain.util.ResultType

class AddCounterUseCase(private val addCounterRepository: AddCounterRepository) {

    suspend fun addCounter(
        addCounterRequestModel: AddCounterRequestModel
    ): ResultType<List<CounterResponseModel>, ErrorModel> {
        return addCounterRepository.addCounter(addCounterRequestModel)
    }
}