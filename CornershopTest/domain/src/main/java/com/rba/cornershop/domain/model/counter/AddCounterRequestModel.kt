package com.rba.cornershop.domain.model.counter

data class AddCounterRequestModel(

    var title: String? = null
)