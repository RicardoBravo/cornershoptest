package com.rba.cornershop.domain.usecase.counter

import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.counter.UpdateCounterRequestModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.repository.counter.UpdateDecrementCounterRepository
import com.rba.cornershop.domain.util.ResultType

class UpdateDecrementCounterUseCase(private val updateDecrementCounterRepository: UpdateDecrementCounterRepository) {

    suspend fun updateCounter(
        updateCounterRequestModel: UpdateCounterRequestModel
    ): ResultType<List<CounterResponseModel>, ErrorModel> {
        return updateDecrementCounterRepository.decrement(updateCounterRequestModel)
    }
}