package com.rba.cornershop.domain.model.error

import com.rba.cornershop.domain.util.ConstantError

data class ErrorModel(
    var code: Int = 0,
    var status: String? = null,
    var message: String? = ConstantError.ERROR
)