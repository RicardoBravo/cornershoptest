package com.rba.cornershop.domain.repository.counter

import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.util.ResultType

interface ListCounterRepository {

    suspend fun listCounter(): ResultType<List<CounterResponseModel>, ErrorModel>
}