package com.rba.cornershop.domain.usecase.counter

import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.counter.UpdateCounterRequestModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.repository.counter.DeleteCounterRepository
import com.rba.cornershop.domain.util.ResultType

class DeleteCounterUseCase(private val deleteCounterRepository: DeleteCounterRepository) {

    suspend fun deleteCounter(
        updateCounterRequestModel: UpdateCounterRequestModel
    ): ResultType<List<CounterResponseModel>, ErrorModel> {
        return deleteCounterRepository.delete(updateCounterRequestModel)
    }
}