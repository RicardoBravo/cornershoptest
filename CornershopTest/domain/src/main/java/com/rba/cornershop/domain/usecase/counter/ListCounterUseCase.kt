package com.rba.cornershop.domain.usecase.counter

import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.repository.counter.ListCounterRepository
import com.rba.cornershop.domain.util.ResultType

class ListCounterUseCase(private val listCounterRepository: ListCounterRepository) {

    suspend fun listCounter(): ResultType<List<CounterResponseModel>, ErrorModel> {
        return listCounterRepository.listCounter()
    }
}