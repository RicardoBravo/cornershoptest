package com.rba.cornershop.domain.repository.counter

import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.counter.UpdateCounterRequestModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.util.ResultType

interface UpdateDecrementCounterRepository {

    suspend fun decrement(updateCounterRequestModel: UpdateCounterRequestModel): ResultType<List<CounterResponseModel>, ErrorModel>
}