package com.rba.cornershop.domain.model.counter

data class UpdateCounterRequestModel(
    var id: String? = null
)