package com.rba.cornershop.domain.repository.counter

import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.counter.UpdateCounterRequestModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.util.ResultType

interface DeleteCounterRepository {

    suspend fun delete(updateCounterRequestModel: UpdateCounterRequestModel): ResultType<List<CounterResponseModel>, ErrorModel>
}