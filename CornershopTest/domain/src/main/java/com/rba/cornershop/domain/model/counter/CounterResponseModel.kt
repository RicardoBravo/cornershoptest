package com.rba.cornershop.domain.model.counter

import android.os.Parcel
import android.os.Parcelable

data class CounterResponseModel(
    var id: String? = null,
    var title: String? = null,
    var count: Int = 0
) : Parcelable {

    companion object {
        @JvmField
        val CREATOR = object : Parcelable.Creator<CounterResponseModel> {
            override fun createFromParcel(parcel: Parcel) = CounterResponseModel(parcel)
            override fun newArray(size: Int) = arrayOfNulls<CounterResponseModel>(size)
        }
    }

    private constructor(parcel: Parcel) : this(
        id = parcel.readString(),
        title = parcel.readString(),
        count = parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(title)
        parcel.writeInt(count)
    }

    override fun describeContents() = 0
}