package com.rba.cornershop.test.main.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rba.cornershop.counterview.listener.CounterViewLimitListener
import com.rba.cornershop.counterview.listener.CounterViewUpDownListener
import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.test.R
import com.rba.cornershop.test.main.listener.MainDeleteListener
import com.rba.cornershop.test.main.listener.MainLimitListener
import com.rba.cornershop.test.main.listener.MainUpDownListener
import com.rba.cornershop.test.util.diffUtil
import com.rba.cornershop.test.util.inflate
import kotlinx.android.synthetic.main.item_main.view.*

class MainAdapter :
    RecyclerView.Adapter<MainAdapter.MainViewHolder>() {

    var list: List<CounterResponseModel> by diffUtil(
        emptyList(),
        areItemsTheSame = { old, new -> old.id == new.id }
    )

    var mainLimitListener: MainLimitListener? = null
    var mainUpDownListener: MainUpDownListener? = null
    var mainDeleteListener: MainDeleteListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder =
        MainViewHolder(parent.inflate(R.layout.item_main, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val movie = list[position]
        holder.bind(movie)
    }

    fun onMainLimitListener(mainLimitListener: MainLimitListener) {
        this.mainLimitListener = mainLimitListener
    }

    fun onMainUpDownListener(mainUpDownListener: MainUpDownListener) {
        this.mainUpDownListener = mainUpDownListener
    }

    fun onMainDeleteListener(mainDeleteListener: MainDeleteListener) {
        this.mainDeleteListener = mainDeleteListener
    }

    fun removeItem(position: Int) {
        notifyItemRemoved(position)
    }

    fun restoreItem(position: Int) {
        notifyItemInserted(position)
    }

    inner class MainViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(counterResponseModel: CounterResponseModel) {

            itemView.setOnLongClickListener {
                mainDeleteListener?.onDelete(adapterPosition)
                return@setOnLongClickListener true
            }

            itemView.descriptionTextView.text = counterResponseModel.title
            itemView.counterView.value = counterResponseModel.count

            itemView.counterView.onCounterViewLimitListener(object :
                CounterViewLimitListener {
                override fun onCounterViewDownLimit() {
                    mainLimitListener?.onDownLimit()
                }

                override fun onCounterViewUpLimit() {
                    mainLimitListener?.onUpLimit()
                }
            })

            itemView.counterView.onCounterViewUpDownListener(object :
                CounterViewUpDownListener {
                override fun onUpValue(value: Int) {
                    mainUpDownListener?.onUpValue(value, adapterPosition)
                }

                override fun onDownValue(value: Int) {
                    mainUpDownListener?.onDownValue(value, adapterPosition)
                }
            })
        }
    }
}