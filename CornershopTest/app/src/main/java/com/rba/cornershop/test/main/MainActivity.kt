package com.rba.cornershop.test.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.rba.cornershop.test.R
import com.rba.cornershop.test.counter.CounterActivity

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.test.main.listener.MainLimitListener
import com.rba.cornershop.test.main.listener.MainUpDownListener
import com.rba.cornershop.test.util.Constant
import com.rba.cornershop.test.util.toast
import androidx.recyclerview.widget.DividerItemDecoration
import com.rba.cornershop.test.main.adapter.MainAdapter
import com.rba.cornershop.test.main.adapter.RecyclerItemTouchHelper
import com.rba.cornershop.test.main.listener.MainDeleteListener
import com.rba.cornershop.test.util.DialogListener
import com.rba.cornershop.test.util.DialogUtil

class MainActivity : AppCompatActivity(), MainLimitListener,
    MainUpDownListener, MainDeleteListener,
    RecyclerItemTouchHelper.RecyclerItemTouchHelperListener, DialogListener {

    private lateinit var mainViewModel: MainViewModel
    private lateinit var mainAdapter: MainAdapter
    private var list: MutableList<CounterResponseModel>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        mainViewModel = ViewModelProviders.of(
            this,
            MainViewModelFactory()
        ).get(MainViewModel::class.java)
        mainViewModel.model.observe(this, Observer(::updateUi))

        init()
    }

    private fun init() {
        mainAdapter = MainAdapter()
        mainAdapter.onMainLimitListener(this)
        mainAdapter.onMainUpDownListener(this)
        mainAdapter.onMainDeleteListener(this)
        counterRecyclerView.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )
        counterRecyclerView.adapter = mainAdapter

        mainViewModel.getData()

        addImageView.setOnClickListener {
            val intent = Intent(this, CounterActivity::class.java)
            startActivityForResult(intent, UPDATE_DATA)
        }

        val itemTouchHelperCallback = RecyclerItemTouchHelper(
            0,
            ItemTouchHelper.LEFT,
            this
        )
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(counterRecyclerView)
    }

    private fun updateUi(model: MainViewModel.UiViewModel) {

        when (model) {
            is MainViewModel.UiViewModel.ShowData -> {
                list = model.counterResponseModelList.toMutableList()
                mainAdapter.list = this.list!!
                total()
            }

            is MainViewModel.UiViewModel.ShowError -> {
                toast(model.errorModel.message.toString())
            }

            is MainViewModel.UiViewModel.ShowErrorDelete -> {
                list?.add(model.index, model.counterResponseModel)
                mainAdapter.list = this.list!!
                mainAdapter.restoreItem(model.index)
            }

            is MainViewModel.UiViewModel.UpdateTotal -> {
                totalTextView.text = getString(R.string.total, model.total.toString())
            }
        }
    }

    override fun onDownLimit() {
        toast(getString(R.string.quantity_min))
    }

    override fun onUpLimit() {
        toast(getString(R.string.quantity_min))
    }

    override fun onDownValue(value: Int, position: Int) {
        mainViewModel.updateDecrement(mainViewModel.requestModel(list!![position].id.toString()))
    }

    override fun onUpValue(value: Int, position: Int) {
        mainViewModel.updateIncrement(mainViewModel.requestModel(list!![position].id.toString()))
    }

    override fun onDelete(currentPosition: Int) {

        val dialogListener: DialogListener = object : DialogListener {
            override fun onClickPositiveButton() {
                delete(currentPosition, list?.get(currentPosition)!!)
            }
        }
        DialogUtil.showProgressDialog(
            this,
            getString(R.string.delete_item, list?.get(currentPosition)?.title), dialogListener
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == UPDATE_DATA && resultCode == Activity.RESULT_OK) {
            val list = data?.getParcelableArrayListExtra<CounterResponseModel>(Constant.DATA)
            this.list = list!!.toMutableList()
            mainAdapter.list = this.list!!
            total()
        }
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        val item = list?.get(viewHolder.adapterPosition)!!

        delete(position, item)
    }

    private fun delete(position: Int, item: CounterResponseModel) {
        list?.removeAt(position)
        mainAdapter.list = this.list!!
        mainAdapter.removeItem(position)

        mainViewModel.delete(
            mainViewModel.requestModel(item.id!!),
            item,
            position
        )
    }

    override fun onClickPositiveButton() {
    }

    private fun total() {
        mainViewModel.total(list!!)
    }

    companion object {
        const val UPDATE_DATA = 1
    }
}
