package com.rba.cornershop.test.main.listener

interface MainUpDownListener {

    fun onUpValue(value: Int, position: Int)

    fun onDownValue(value: Int, position: Int)
}