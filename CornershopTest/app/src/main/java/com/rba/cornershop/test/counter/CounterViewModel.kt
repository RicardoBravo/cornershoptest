package com.rba.cornershop.test.counter

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rba.cornershop.domain.model.counter.AddCounterRequestModel
import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.usecase.counter.AddCounterUseCase
import com.rba.cornershop.domain.util.ResultType
import com.rba.cornershop.test.util.ScopedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CounterViewModel(private val addCounterUseCase: AddCounterUseCase) : ScopedViewModel() {

    private val mutableModel = MutableLiveData<UiViewModel>()
    val model: LiveData<UiViewModel>
        get() {
            return mutableModel
        }

    init {
        initScope()
    }

    fun saveData(addCounterRequestModel: AddCounterRequestModel) {
        UiViewModel.ShowLoading()
        GlobalScope.launch(Dispatchers.Main) {
            when (val result =
                addCounterUseCase.addCounter(addCounterRequestModel)) {
                is ResultType.Success -> mutableModel.value = UiViewModel.ShowData(result.value)
                is ResultType.Error -> mutableModel.value = UiViewModel.ShowError(result.value)
            }
        }
        UiViewModel.HideLoading()
    }

    fun requestModel(title: String): AddCounterRequestModel {
        return AddCounterRequestModel(title)
    }

    sealed class UiViewModel {
        class ShowData(val counterResponseModelList: List<CounterResponseModel>) : UiViewModel()
        class ShowError(val errorModel: ErrorModel) : UiViewModel()
        class ShowLoading : UiViewModel()
        class HideLoading : UiViewModel()
    }

    override fun onCleared() {
        destroyScope()
        super.onCleared()
    }
}