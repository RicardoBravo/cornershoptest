package com.rba.cornershop.test.counter

import android.os.Bundle
import android.view.KeyEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.rba.cornershop.test.R

import kotlinx.android.synthetic.main.activity_counter.*
import kotlinx.android.synthetic.main.content_counter.*
import android.app.Activity
import android.content.Intent
import android.view.MenuItem
import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.test.util.Constant
import com.rba.cornershop.test.util.ProgressUtil
import com.rba.cornershop.test.util.toast

class CounterActivity : AppCompatActivity() {

    private lateinit var counterViewModel: CounterViewModel
    private var list: ArrayList<CounterResponseModel>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_counter)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        counterViewModel = ViewModelProviders.of(
            this,
            CounterViewModelFactory()
        ).get(CounterViewModel::class.java)
        counterViewModel.model.observe(this, Observer(::updateUi))

        counterButton.setOnClickListener {
            if (!counterTextInputEditText.text.isNullOrEmpty()) {
                counterViewModel.saveData(counterViewModel.requestModel(title = counterTextInputEditText.text.toString()))
            } else {
                counterTextInputLayout.error = getString(R.string.field_not_empty)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        validateBackIntent()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            validateBackIntent()
            return false
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun updateUi(model: CounterViewModel.UiViewModel) {

        when (model) {
            is CounterViewModel.UiViewModel.ShowData -> {
                list = ArrayList(model.counterResponseModelList)
                backIntentOk()
            }

            is CounterViewModel.UiViewModel.ShowError -> {
                toast(model.errorModel.message.toString())
            }

            is CounterViewModel.UiViewModel.ShowLoading -> {
                ProgressUtil.showProgressDialog(this)
            }

            is CounterViewModel.UiViewModel.HideLoading -> {
                ProgressUtil.hideProgressDialog()
            }
        }
    }

    private fun validateBackIntent() {
        if (list == null) {
            backIntentCancel()
        } else {
            backIntentOk()
        }
    }

    private fun backIntentOk() {
        val intent = Intent()
        intent.putParcelableArrayListExtra(Constant.DATA, list)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun backIntentCancel() {
        val intent = Intent()
        setResult(Activity.RESULT_CANCELED, intent)
        finish()
    }
}
