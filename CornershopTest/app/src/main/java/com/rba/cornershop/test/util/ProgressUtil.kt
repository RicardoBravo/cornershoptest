package com.rba.cornershop.test.util

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import com.rba.cornershop.test.R

object ProgressUtil {

    private var alertDialog: AlertDialog? = null

    fun showProgressDialog(context: Context) {
        if (alertDialog == null) {
            val builder = AlertDialog.Builder(context)
            val inflater: LayoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = inflater.inflate(R.layout.item_progress, null)
            builder.setView(view)
                .setCancelable(false)
            alertDialog = builder.create()
            alertDialog!!.show()
        }
    }

    fun hideProgressDialog() {
        if (alertDialog != null && alertDialog!!.isShowing) {
            alertDialog!!.dismiss()
        }
    }
}