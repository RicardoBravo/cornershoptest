package com.rba.cornershop.test.util

interface DialogListener {
    fun onClickPositiveButton()
}