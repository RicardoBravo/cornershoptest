package com.rba.cornershop.test.main.listener

interface MainLimitListener {

    fun onDownLimit()

    fun onUpLimit()
}