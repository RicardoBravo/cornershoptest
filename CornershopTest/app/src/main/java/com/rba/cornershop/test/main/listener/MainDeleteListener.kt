package com.rba.cornershop.test.main.listener

interface MainDeleteListener {

    fun onDelete(position: Int)
}