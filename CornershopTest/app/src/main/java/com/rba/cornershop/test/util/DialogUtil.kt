package com.rba.cornershop.test.util

import android.app.AlertDialog
import android.content.Context
import com.rba.cornershop.test.R

object DialogUtil {

    private var alertDialog: AlertDialog? = null

    fun showProgressDialog(context: Context, message: String, dialogListener: DialogListener?) {
        if (alertDialog == null) {
            val builder = AlertDialog.Builder(context)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton(context.getString(R.string.no)) { dialog, _ -> dialog.cancel() }
                .setPositiveButton(
                    context.getString(R.string.yes)
                ) { _, _ -> dialogListener?.onClickPositiveButton() }
            alertDialog = builder.create()
        }

        if (alertDialog != null && !alertDialog!!.isShowing) {
            alertDialog!!.show()
        }
    }

    fun hideProgressDialog() {
        if (alertDialog != null && alertDialog!!.isShowing) {
            alertDialog!!.dismiss()
        }
    }
}