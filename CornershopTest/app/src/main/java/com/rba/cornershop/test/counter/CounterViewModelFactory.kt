package com.rba.cornershop.test.counter

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rba.cornershop.data.repository.counter.AddCounterDataRepository
import com.rba.cornershop.domain.usecase.counter.AddCounterUseCase

class CounterViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(CounterViewModel::class.java)) {
            val addCounterUseCase =
                AddCounterUseCase(AddCounterDataRepository())
            CounterViewModel(addCounterUseCase) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}