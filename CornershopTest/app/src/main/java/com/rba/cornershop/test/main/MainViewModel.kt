package com.rba.cornershop.test.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rba.cornershop.domain.model.counter.CounterResponseModel
import com.rba.cornershop.domain.model.counter.UpdateCounterRequestModel
import com.rba.cornershop.domain.model.error.ErrorModel
import com.rba.cornershop.domain.usecase.counter.DeleteCounterUseCase
import com.rba.cornershop.domain.usecase.counter.ListCounterUseCase
import com.rba.cornershop.domain.usecase.counter.UpdateDecrementCounterUseCase
import com.rba.cornershop.domain.usecase.counter.UpdateIncrementCounterUseCase
import com.rba.cornershop.domain.util.ResultType
import com.rba.cornershop.test.util.ScopedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainViewModel(
    private val updateIncrementCounterUseCase: UpdateIncrementCounterUseCase,
    private val updateDecrementCounterUseCase: UpdateDecrementCounterUseCase,
    private val listCounterUseCase: ListCounterUseCase,
    private val deleteCounterUseCase: DeleteCounterUseCase
) : ScopedViewModel() {

    private val mutableModel = MutableLiveData<UiViewModel>()
    val model: LiveData<UiViewModel>
        get() {
            return mutableModel
        }

    init {
        initScope()
    }

    fun getData() {
        UiViewModel.ShowLoading()
        GlobalScope.launch(Dispatchers.Main) {
            when (val result =
                listCounterUseCase.listCounter()) {
                is ResultType.Success -> mutableModel.value = UiViewModel.ShowData(result.value)
                is ResultType.Error -> mutableModel.value = UiViewModel.ShowError(result.value)
            }
        }
        UiViewModel.HideLoading()
    }

    fun updateIncrement(updateCounterRequestModel: UpdateCounterRequestModel) {
        UiViewModel.ShowLoading()
        GlobalScope.launch(Dispatchers.Main) {
            when (val result =
                updateIncrementCounterUseCase.updateCounter(
                    updateCounterRequestModel
                )) {
                is ResultType.Success -> mutableModel.value = UiViewModel.ShowData(result.value)
                is ResultType.Error -> mutableModel.value = UiViewModel.ShowError(result.value)
            }
        }
        UiViewModel.HideLoading()
    }

    fun updateDecrement(updateCounterRequestModel: UpdateCounterRequestModel) {
        UiViewModel.ShowLoading()
        GlobalScope.launch(Dispatchers.Main) {
            when (val result =
                updateDecrementCounterUseCase.updateCounter(
                    updateCounterRequestModel
                )) {
                is ResultType.Success -> mutableModel.value = UiViewModel.ShowData(result.value)
                is ResultType.Error -> mutableModel.value = UiViewModel.ShowError(result.value)
            }
        }
        UiViewModel.HideLoading()
    }

    fun delete(
        updateCounterRequestModel: UpdateCounterRequestModel,
        counterResponseModel: CounterResponseModel,
        index: Int
    ) {
        UiViewModel.ShowLoading()
        GlobalScope.launch(Dispatchers.Main) {
            when (val result =
                deleteCounterUseCase.deleteCounter(
                    updateCounterRequestModel
                )) {
                is ResultType.Success -> mutableModel.value = UiViewModel.ShowData(result.value)
                is ResultType.Error -> mutableModel.value =
                    UiViewModel.ShowErrorDelete(result.value, counterResponseModel, index)
            }
        }
        UiViewModel.HideLoading()
    }

    fun requestModel(id: String): UpdateCounterRequestModel {
        return UpdateCounterRequestModel(id)
    }

    fun total(list: List<CounterResponseModel>) {
        var total = 0
        list.map {
            total += it.count
        }
        mutableModel.value = UiViewModel.UpdateTotal(total)
    }

    fun onLongClick(id: String, counterResponseModel: CounterResponseModel, index: Int) {
        delete(requestModel(id), counterResponseModel, index)
    }

    sealed class UiViewModel {
        class ShowData(val counterResponseModelList: List<CounterResponseModel>) : UiViewModel()
        class ShowError(val errorModel: ErrorModel) : UiViewModel()
        class ShowErrorDelete(
            val errorModel: ErrorModel,
            val counterResponseModel: CounterResponseModel,
            val index: Int
        ) : UiViewModel()

        class UpdateTotal(val total: Int) : UiViewModel()
        class ShowLoading : UiViewModel()
        class HideLoading : UiViewModel()
    }

    override fun onCleared() {
        destroyScope()
        super.onCleared()
    }
}