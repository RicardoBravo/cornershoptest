package com.rba.cornershop.test.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rba.cornershop.data.repository.counter.DeleteCounterDataRepository
import com.rba.cornershop.data.repository.counter.ListCounterDataRepository
import com.rba.cornershop.data.repository.counter.UpdateDecrementCounterDataRepository
import com.rba.cornershop.data.repository.counter.UpdateIncrementCounterDataRepository
import com.rba.cornershop.domain.usecase.counter.DeleteCounterUseCase
import com.rba.cornershop.domain.usecase.counter.ListCounterUseCase
import com.rba.cornershop.domain.usecase.counter.UpdateDecrementCounterUseCase
import com.rba.cornershop.domain.usecase.counter.UpdateIncrementCounterUseCase

class MainViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            val updateIncrementCounterUseCase =
                UpdateIncrementCounterUseCase(
                    UpdateIncrementCounterDataRepository()
                )
            val updateDecrementCounterUseCase =
                UpdateDecrementCounterUseCase(
                    UpdateDecrementCounterDataRepository()
                )
            val listCounterUseCase =
                ListCounterUseCase(
                    ListCounterDataRepository()
                )
            val deleteCounterUseCase =
                DeleteCounterUseCase(
                    DeleteCounterDataRepository()
                )
            MainViewModel(
                updateIncrementCounterUseCase,
                updateDecrementCounterUseCase,
                listCounterUseCase,
                deleteCounterUseCase
            ) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}