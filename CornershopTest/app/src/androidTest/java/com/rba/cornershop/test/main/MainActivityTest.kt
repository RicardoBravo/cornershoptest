package com.rba.cornershop.test.main

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.rba.cornershop.test.R
import com.rba.cornershop.test.base.BaseUiTestActivity
import com.rba.cornershop.test.util.MatcherUtil.withBackgroundColor
import com.rba.cornershop.test.util.MatcherUtil.withCornerRadius
import com.rba.cornershop.test.util.MatcherUtil.withTextColor
import com.rba.cornershop.test.util.MatcherUtil.withTextSize
import org.junit.Test

class MainActivityTest : BaseUiTestActivity(MainActivity::class.java) {

    private val cornerRadius by lazy { context.resources.getDimensionPixelSize(R.dimen.dp_0) }

    @Test
    fun testCounterTextView() {
        val id = R.id.textView

        onView(withId(id)).check(matches(isDisplayed()))
        onView(withId(id)).check(matches(isEnabled()))
        onView(withId(id)).check(matches(withTextColor(R.color.colorText)))
        onView(withId(id)).check(matches(withTextSize(context.resources.getDimensionPixelSize(R.dimen.sp_18))))
    }

    @Test
    fun testTotalTextView() {
        val id = R.id.totalTextView

        onView(withId(id)).check(matches(isDisplayed()))
        onView(withId(id)).check(matches(isEnabled()))
        onView(withId(id)).check(matches(withTextColor(R.color.colorText)))
        onView(withId(id)).check(matches(withTextSize(context.resources.getDimensionPixelSize(R.dimen.sp_20))))
    }

    @Test
    fun testRecyclerView() {
        val id = R.id.counterRecyclerView

        onView(withId(id)).check(matches(isDisplayed()))
        onView(withId(id)).check(matches(isEnabled()))
        onView(withId(id)).check(matches(withBackgroundColor(R.color.colorWhite)))
        onView(withId(id)).check(matches(withCornerRadius(cornerRadius)))
    }
}